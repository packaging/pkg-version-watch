# multiple packages

```
pkgs:
- name: knot
  latest: prev
- name: knot-resolver

set_latest: 'all'
set_latest: 'all:current'
set_latest: 'all:prev'
set_latest: 'all:weirdrepo'
set_latest: 'knot-dns'
set_latest: 'knot-dns:current'
set_latest: 'knot-dns:prev'
```

- [ ] inherit pkgs from parents of all levels
- [ ] probably needs jinja templating for nvconf values with rpmrepo fetcher (pkg in repo URL).


# upstream version(s)

- github upstream entry (current, previous vs v6, v5)
- can mark release as upstream version
- per package
