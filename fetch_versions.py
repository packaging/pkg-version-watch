#!/usr/bin/env python3
# Fetch package versions as defined in versions.yaml
# using nvchecker into versions.json

import asyncio
import json
import yaml
import re

from nvchecker import core
from nvchecker import __main__ as nvc_main
from nvchecker.util import Entries, RawResult


VERSIONS_INPUT = 'versions.yaml'
VERSIONS_OUTPUT = 'versions.json'


def fetch_versions():
    with open(VERSIONS_INPUT, 'r') as f:
        config = yaml.safe_load(f)

    output = config.copy()
    for project in output['projects']:
        print(f"Fetching versions for {project['name']}")
        render_project_config(project)
        nvconf_project = get_project_nvconf(project)
        versions = asyncio.run(get_versions_nvchecker(nvconf_project))
        apply_project_versions(project, versions)

    with open(VERSIONS_OUTPUT, 'w') as f:
        json.dump(output, f)

    print(yaml.dump(output))


async def run_nvchecker(
    entries: Entries,
    max_concurrency: int = 20,
) -> dict:
    task_sem = asyncio.Semaphore(max_concurrency)
    result_q: asyncio.Queue[RawResult] = asyncio.Queue()
    keymanager = core.KeyManager(None)
    dispatcher = core.setup_httpclient()
    entry_waiter = core.EntryWaiter()
    futures = dispatcher.dispatch(
        entries, task_sem, result_q,
        keymanager, entry_waiter, 1, {},
    )

    oldvers = {}
    result_coro = core.process_result(oldvers, result_q, entry_waiter)
    runner_coro = core.run_tasks(futures)

    vers, _has_failures = await nvc_main.run(result_coro, runner_coro)
    return vers


async def get_versions_nvchecker(nvchecker_entries):
    vers = await run_nvchecker(nvchecker_entries)
    # extract version strings from RichResult
    versions = {n: str(v) for n, v in vers.items()}
    return versions


def render_project_config(project):
    ensure_id(project)
    for sources_group in project['sources_groups']:
        ensure_id(sources_group)
        for source in sources_group['sources']:
            ensure_id(source)
            for distro in source['distros']:
                ensure_id(distro)
                nvconf_distro = distro['nvconf']
                for distro_release in distro['releases']:
                    nvconf_release = distro_release.get('nvconf', {})
                    nvconf = nvconf_distro.copy()
                    nvconf.update(nvconf_release)
                    distro_release['nvconf'] = nvconf


def get_project_nvconf(project):
    nvconf = {}
    for sources_group in project['sources_groups']:
        for source in sources_group['sources']:
            for distro in source['distros']:
                for release in distro['releases']:
                    nvconf_section = get_nvconf_section(
                        sources_group['id'], source['id'],
                        distro['id'], release['name'])
                    nvconf[nvconf_section] = release['nvconf']
    return nvconf


def apply_project_versions(project, versions):
    for sources_group in project['sources_groups']:
        for source in sources_group['sources']:
            for distro in source['distros']:
                for release in distro['releases']:
                    nvconf_section = get_nvconf_section(
                        sources_group['id'], source['id'],
                        distro['id'], release['name'])
                    version = versions.get(nvconf_section, 'X')
                    release['version'] = version
                    release.pop('nvconf', None)
                distro.pop('nvconf', None)


def get_nvconf_section(sources_group_id, source_id, distro_id, distro_version):
    section = "%s_%s_%s_%s" % (sources_group_id, source_id, distro_id, distro_version)
    section = name2id(section)
    return section


def name2id(name : str) -> str:
    return re.sub(r'\W', '-', name.lower())


def ensure_id(cfg) -> str:
    name = cfg['name']
    if 'id' not in cfg:
        id = name2id(name)
        cfg['id'] = id
    return cfg['id']


if __name__ == '__main__':
    fetch_versions()
