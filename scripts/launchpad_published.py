#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
import sys

url = "https://launchpad.net/~cz.nic-labs/+archive/ubuntu/knot-resolver/+packages"

req = requests.get(url)
soup = BeautifulSoup(req.text, "html.parser")
packages_list = soup.find(id='packages_list')

published = []
pending = []
for tr in packages_list.find_all('tr'):
    tds = list(tr.find_all('td'))
    if len(tds) != 8:
        continue
    source = tds[0].text.strip()
    status = tds[4].text.strip()

    if status == 'Published':
        published.append(source)
    else:
        pending.append((source, status))

if len(pending) == 0:
    print("All builds published.")
else:
    print("%s builds not published:" % len(pending))
    for src, status in pending:
        print("    %s: %s" % (src, status))
    sys.exit(1)
