# pkg-version-watch

Prototyping package versions monitor/dashboard/tracker.

# usage

Run

```
./fetch_versions.py
```

to fetch versions as defined in `versions.yaml` into `versions.json`.

Run

```
./render_versions.py
```

to render static HTML page from `versions.json` into `out/` directory.
